#!/bin/bash

#allow this script to be sourced interactively without 
#clobbering the terminal
if [[ ! $- == *i* ]]; then
	set -o nounset
fi

function clean
{
	#http://stackoverflow.com/a/17902999/229631
	if [ -n "$(ls -A $DOWNLOADS)" ]; then
		notice Cleaning...
		for d in `ls -d $DOWNLOADS*/`; do
			pushd $d
			(make clean || true) &> /dev/null
			popd
		done
	fi
}

function notice
{
        echo
        yellow=`tput setaf 3`
        reset=`tput sgr 0`
        echo "${yellow}${1}${reset}"
        echo
}

function apt_prereqs()
{
	if [ ! -z ${TRAVIS+x} ]; then #if we're running under Travis-CI, turn this into a no-op as dependencies are already handled
	        if [ ! -e ".prereqs-done" ]; then
		        #http://serverfault.com/questions/20747/find-last-time-update-was-performed-with-apt-get
       			#http://stackoverflow.com/questions/8903239/how-to-calculate-time-difference-in-bash-script
        		last=$(stat -c %y /var/lib/apt/periodic/update-success-stamp || echo 0)
        		last=$(date --date="$last" +"%s")
        		now=$(date +"%s")
        		difference=$(($now-$last))

        		if [ $difference -gt 21600 ]; then
                		notice "Might prompt for your sudo password now"
                		sudo apt-get update
				sudo apt-get -y --no-install-recommends install tmux git build-essential make automake autoconf libtool texinfo libexpat-dev pkg-config gettext bison flex && touch .prereqs-done
			fi
        	fi
	fi
}

export JOBS=`nproc`
export WORKSPACE=`pwd`/workspace/
export DOWNLOADS=$WORKSPACE/downloads/
export HOST_PREFIX=$WORKSPACE/host/
export HOST_PKG_CONFIG_PATH=$HOST_PREFIX/lib/pkgconfig
export PKG_CONFIG_PATH=$HOST_PKG_CONFIG_PATH
export BUILD_PREFIX=$WORKSPACE/dependencies/
export CROSS_PKG_CONFIG_PATH=$BUILD_PREFIX/lib/pkgconfig
export CROSS_TOOLCHAIN=armv7-rpi2-linux-gnueabihf #TODO: make this configurable
export PKG_CONFIG_LIBDIR=


function host_has()
{
        PKG_CONFIG_PATH=$HOST_PKG_CONFIG_PATH pkg-config --exists $1
}


function target_has()
{
        PKG_CONFIG_PATH=$CROSS_PKG_CONFIG_PATH pkg-config --exists $1
}

function set_cross_environment()
{
        export ORIGINAL_PATH=$PATH
        export PKG_CONFIG_PATH=$CROSS_PKG_CONFIG_PATH
        mkdir -pv $BUILD_PREFIX
        export PATH=$PATH:$HOME/x-tools/$CROSS_TOOLCHAIN/bin
        export ARCH=arm
        export CC=$CROSS_TOOLCHAIN-gcc
        export HOST=$CROSS_TOOLCHAIN
}

function unset_cross_environment()
{
        unset BUILD_PREFIX
        unset HOST_PREFIX
        unset HOST_PKG_CONFIG_PATH
        unset PKG_CONFIG_PATH
        unset ARCH
        unset CC
        unset HOST
        export PATH=$ORIGINAL_PATH
}


function download()
{
        pushd $DOWNLOADS
        wget -c $1
        #http://stackoverflow.com/questions/1199613/extract-filename-and-path-from-url-in-bash-script
        FILENAME=`printf -- "%s" "${1##*/}"`
        tar axf $FILENAME
        popd
}
