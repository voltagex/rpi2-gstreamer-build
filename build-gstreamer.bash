#!/bin/bash
set -o errexit
set -o nounset

source common.bash

mkdir -pv $WORKSPACE $DOWNLOADS $HOST_PREFIX

#http://stackoverflow.com/a/13864829/229631
if [ ! -z ${TRAVIS+x} ]; then #pull down a pre built cross-compiler for Travis-CI
	pushd $HOME
	wget -c https://github.com/voltagex/rpi-crosscompiler-travis/releases/download/v0.4/armv7-rpi2-linux-gnueabihf.tar.xz
	tar axf armv7-rpi2-linux-gnueabihf.tar.xz
	popd
fi

notice "Checking prerequisites"
apt_prereqs
clean

if ! host_has zlib; then
	notice "Building host zlib"
	download http://zlib.net/zlib-1.2.8.tar.xz
	pushd $DOWNLOADS/zlib*
	./configure --prefix=$HOST_PREFIX >> build.log
	(make && make install && make clean) >> build.log
	popd
else
	notice "Host zlib installed"
fi

if ! host_has libffi; then
	notice "Building host libffi"
	download ftp://sourceware.org/pub/libffi/libffi-3.2.1.tar.gz
	pushd $DOWNLOADS/libffi*
	./configure --prefix=$HOST_PREFIX >> build.log
	(make -j $JOBS && make install && make clean) >> build.log
	popd
else
	notice "Host libffi installed"
fi

if ! host_has glib-2.0; then
	notice "Building host glib-2.0"
	#watch the version number used here
	#http://mandriva.598463.n5.nabble.com/OM-Cooker-Cooker-OMCC-doesn-t-open-drakrpm-edit-media-core-dumped-tp5720974p5720981.html
	#use https://abf.io/openmandriva/glib2.0/raw/master/glib-2.46.0-revert_quark_optim.patch or
	#download http://ftp.gnome.org/mirror/gnome.org/sources/glib/2.45/glib-2.45.8.tar.xz
	download http://ftp.acc.umu.se/pub/GNOME/sources/glib/2.46/glib-2.46.1.tar.xz
	pushd $DOWNLOADS/glib-2.46.1*
	if [ ! -e ".glib-quark-patched" ]; then
		patch -p1 < $WORKSPACE/../glib-2.46.0-revert_quark_optim.patch && touch .glib-quark-patched
	fi
	./configure --prefix=$HOST_PREFIX >> build.log
	(make -j $JOBS && make install && make clean) >> build.log

	popd
else
	notice "Host glib2 installed"
fi

export PATH=$PATH:$HOST_PREFIX/bin
set_cross_environment

if ! target_has zlib; then
	pushd $DOWNLOADS/zlib*
	./configure --prefix=$BUILD_PREFIX 
	(make && make install && make clean)
	popd
else
	notice "Target zlib is installed"
fi

if ! target_has libffi; then
	notice "Building target libffi"
	pushd $DOWNLOADS/libffi*
	./configure --prefix=$BUILD_PREFIX --host=$HOST >> build.log
	(make -j $JOBS && make install && make clean) >> build.log
	popd
else
	notice "Target libffi is installed"
fi

if [ ! -e $BUILD_PREFIX/lib/libgettextsrc.la ]; then
	notice "Building target gettext"
	download http://gnu.uberglobalmirror.com/gettext/gettext-latest.tar.xz
	pushd $DOWNLOADS/gettext*
	./configure --prefix=$BUILD_PREFIX --host=$HOST >> build.log
	(make -j $JOBS && make install && make clean) >> build.log
	popd
else
	notice "Target gettext is installed"
fi


if ! target_has glib-2.0; then
	notice "Building target glib2"
	pushd $DOWNLOADS/glib-2.46.1*
	#http://www.blogs.soctel.in/checking-for-preceeding-underscore-in-symbols-configure-error/
	#https://www.mail-archive.com/gstreamer-embedded@lists.sourceforge.net/msg00645.html
	glib_cv_stack_grows=no ac_cv_func_posix_getpwuid_r=yes glib_cv_uscore=no ac_cv_func_posix_getgrgid_r=yes ./configure --prefix=$BUILD_PREFIX --host=$HOST
	(make -j $JOBS && make install) >> build.log
	popd
else
	notice "Target glib2 is installed"
fi

if ! target_has gstreamer-1.0; then
	notice "Building target gstreamer"
	download http://gstreamer.freedesktop.org/src/gstreamer/gstreamer-1.6.0.tar.xz

	#no idea, it silences some warnings and makes symbol exports work
	if [ ! -e "$BUILD_PREFIX/include/gstreamer-1.0/gst/gstconfig.h" ]; then
		mkdir -pv $BUILD_PREFIX/include/gstreamer-1.0/gst/
		ln -s $BUILD_PREFIX/lib/gstreamer-1.0/include/gst/gstconfig.h $BUILD_PREFIX/include/gstreamer-1.0/gst/gstconfig.h || true
	fi

	pushd $DOWNLOADS/gstreamer*
	./configure --prefix=$BUILD_PREFIX --host=$HOST --exec-prefix=$BUILD_PREFIX --disable-benchmarks --disable-examples --disable-tests
	(make && make install && make clean)
	popd
else
	notice "Target gstreamer is installed"
fi

if ! target_has gstreamer-plugins-base-1.0; then
	notice "Building target gst-plugins-base"
	download http://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-1.6.0.tar.xz
	pushd $DOWNLOADS/gst-plugins-base*
	./configure --prefix=$BUILD_PREFIX --exec-prefix=$BUILD_PREFIX --host=$HOST >> build.log
	(make -j $JOBS && make install && make clean) >> build.log
else
	notice "Target gst-plugins-base is installed"
fi

#todo: check if this is already installed
if [ ! -d $DOWNLOADS/gst-omx ]; then
	git clone --depth=1 git://anongit.freedesktop.org/gstreamer/gst-omx $DOWNLOADS/gst-omx
fi

if [ ! -d $DOWNLOADS/rpi-omx-headers-only ]; then
	#https://github.com/matthiasbock/gstreamer/issues/3
	git clone --depth=1 https://github.com/voltagex/rpi-omx-headers-only $DOWNLOADS/rpi-omx-headers-only
fi

if [ ! -e "$BUILD_PREFIX/lib/gstreamer-1.0/libgstomx.so" ]; then
	notice "Building gst-omx"
	pushd $DOWNLOADS/gst-omx
	./autogen.sh --noconfigure >> build.log
	./configure  --prefix=$BUILD_PREFIX --host=$HOST --with-omx-header-path=$DOWNLOADS/rpi-omx-headers-only/opt/vc/include/IL --with-omx-target=rpi >> build.log
	(make && make install && make clean) >> build.log
	popd
else
	notice "Target gst-omx is already installed"
fi

#https://www.raspberrypi.org/forums/viewtopic.php?f=38&t=6852&start=25
#remember to set GST_OMX_CONFIG_DIR
cp $DOWNLOADS/gst-omx/config/rpi/gstomx.conf $BUILD_PREFIX/


if ! target_has gstreamer-plugins-bad-1.0; then
	notice "Building target gst-plugins-bad"
	download http://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-1.6.0.tar.xz
	pushd $DOWNLOADS/gst-plugins-bad*
	./configure --prefix=$BUILD_PREFIX --exec-prefix=$BUILD_PREFIX --host=$HOST >> build.log
	(make -j $JOBS && make install && make clean) >> build.log
fi

if ! target_has gstreamer-plugins-good-1.0; then
	notice "Building target gst-plugins-good"
	download http://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-1.6.0.tar.xz
	pushd $DOWNLOADS/gst-plugins-good*
	./configure --prefix=$BUILD_PREFIX --exec-prefix=$BUILD_PREFIX --host=$HOST >> build.log
	(make -j $JOBS && make install && make clean) >> build.log
fi


#crazy line for debugging
#rm ~/.cache/gstreamer-1.0/registry*; GST_OMX_CONFIG_DIR=~/dependencies GST_DEBUG=*:2 PATH=$PATH:~/dependencies/bin LD_LIBRARY_PATH=~/dependencies/lib/ GST_PLUGIN_SCANNER=~/dependencies/libexec/gstreamer-1.0/gst-plugin-scanner gst-inspect-1.0 ~/dependencies/lib/gstreamer-1.0/libgstomx.so

#if you're not on raspbian, you'll probably be missing /opt/vc/lib/libopenmaxil.so
#git clone --depth=1 https://github.com/raspberrypi/firmware, shake your head at binary blobs and copy the opt/ directory to /opt
