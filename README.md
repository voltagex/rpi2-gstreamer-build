# rpi2-gstreamer-build
Build scripts for the latest version of gstreamer for a Raspberry Pi (2)

## TODO

* https://www.raspberrypi.org/forums/viewtopic.php?p=293960
```
GST_OMX_CONFIG_DIR=~/dependencies GST_DEBUG=*:1 \ 
PATH=$PATH:~/dependencies/bin \ 
LD_LIBRARY_PATH=~/dependencies/lib/ 
GST_PLUGIN_SCANNER=~/dependencies/libexec/gstreamer-1.0/gst-plugin-scanner \ 
gst-launch-1.0 dvbsrc bandwidth=7 frequency=191500000 ! tsdemux ! omxh264dec
``` 
fails with
```
0:00:00.100300658  2982   0xc39ae0 ERROR omx gstomx.c:100:gst_omx_core_acquire: Failed to load libbcm_host.so
ERROR: Pipeline doesn't want to pause.
ERROR: from element /GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0: Could not initialize supporting library.
Additional debug info:
gstvideodecoder.c(2571): gst_video_decoder_change_state (): /GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0:
Failed to open decoder
```

* Figure out why http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good-plugins/html/gst-plugins-good-plugins-plugin-soup.html isn't being built
* Silence / fix `stat: cannot stat ‘/var/lib/apt/periodic/update-success-stamp’: No such file or directory`
* Restore environment correctly on script exit / interrupt
